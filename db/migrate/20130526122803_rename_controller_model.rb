class RenameControllerModel < ActiveRecord::Migration
  def change
    change_table :conditions do |t|
      t.rename :controller_model, :controller_generation
    end
  end
end
