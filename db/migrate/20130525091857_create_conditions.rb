class CreateConditions < ActiveRecord::Migration
  def change
    create_table :conditions do |t|
      t.integer :alarm_id, :null => false
      t.integer :user_id, :null => false
      t.string :controller_model
      t.string :robot_model
      t.text :description
      t.text :diagnostic_info
      t.boolean :solved
      t.string :part_replaced
      t.timestamps
    end
  end
end
