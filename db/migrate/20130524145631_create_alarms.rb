class CreateAlarms < ActiveRecord::Migration
  def up
      create_table :alarms do |t|
        t.string :alarm_no
        t.text :error_msg
        t.string :severity
        # Add fields that let Rails automatically keep track
        # of when movies are added or modified:
        t.timestamps
      end
    end

    def down
      drop_table :alarms # deletes the whole table and all its data!
    end
end
