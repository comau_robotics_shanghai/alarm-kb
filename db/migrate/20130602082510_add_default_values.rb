class AddDefaultValues < ActiveRecord::Migration
  def change
    change_column :conditions, :solved, :boolean, :default => false
    change_column :conditions, :part_replaced, :string, :default => ""
  end
end
