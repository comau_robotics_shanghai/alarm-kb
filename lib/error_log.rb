require 'carrierwave/mount'
 
class ErrorLog
  extend CarrierWave::Mount
  mount_uploader :error_log, ErrorLogUploader
  
  def persist_and_send_mail(user_id, date, time, range, contents, original_filename)
    tempfile = "/tmp/#{Time.now.to_s.gsub(/[\ \-\:\+]/, '_')}.log"
    File.open(tempfile, "w+") do |f|
      f.write(contents)
      error_log.store!(f)
    end
    File.delete(tempfile)
    
    # send mail to admin
    LogAnalyzerMailer.analyze_log_email(user_id, date, time, range, original_filename, error_log.url).deliver
  end
end