Alarmknowledgebase::Application.routes.draw do
  devise_for :users

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products
  match 'alarms/search' => 'alarms#search', :method => :get, :as => :search_alarm
  resources :alarms do
    get 'page/:page', :action => :index, :on => :collection
  end
  
  match 'robots/search' => 'robots#search', :method => :get, :as => :search_robot
  match 'robots/axes' => 'robots#axes', :method => :get, :as => :axes
  resources :robots do
    get 'page/:page', :action => :index, :on => :collection
  end
  
  match 'parts/search' => 'parts#search', :method => :get, :as => :search_part
  resources :parts
  resources :parts do
    get 'page/:page', :action => :index, :on => :collection
  end
  
  match 'diagnoses/search' => 'diagnoses#search', :method => :get, :as => :search_diagnosis
  match 'diagnoses/logfile' => 'diagnoses#logfile', :as => :diagnosis_logfile
  match 'diagnoses/analyze' => 'diagnoses#analyze', :as => :diagnosis_analyze
  match 'diagnoses/part' => 'diagnoses#part', :method => :get, :as => :diagnosis_part
  match 'diagnoses/manifests/get_replacement_by_part_name' => 'manifests#get_replacement_by_part_name'
  match 'diagnoses/robots/axes' => 'robots#axes'
  resources :diagnoses
  resources :diagnoses do
    get 'page/:page', :action => :index, :on => :collection
    get 'page/:page', :action => :search, :on => :collection
  end
  
  match '/uploads/diagnosis/diagnosing_diagram/:id/:filename' => 'gridfs#serve'
  match '/uploads/manifest/replacement_pdf/:id/:filename' => 'gridfs#serve'

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'static_pages#home'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
