class Diagnosis
  # ALL_CONTROLLER_GENERATIONS = ["C3G", "C4G", "C5G"]
  # ALL_ROBOT_MODELS = ["Arc4", "SIX", "NS", "NM", "NJ 60", "NJ4 90", "NJ 110-130", "NJ 165-220", "NJ4", "PAL", "NJ 290-370", "NJ 370-500", "NX", "PAL 470 3.1"]
  # ALL_PARTS_REPLACED = ["MOTOR", "DSP", "CABLE"]
  include Mongoid::Document
  include Mongoid::Timestamps
  
  # has_many :alarms
  field :alarms, :type => Array
  field :note, :type => String, localize: true
  field :diagnosing_diagram, :type => String
  embeds_many :solutions
  
  accepts_nested_attributes_for :solutions, :allow_destroy => true
  
  default_scope order_by(:created_at => :asc)
  
  mount_uploader :diagnosing_diagram, DiagnosingDiagramUploader
  
  #def alarm_attributes=(alarm_attributes)
  #end
  
  # validates_presence_of :alarm_id
  # validates_presence_of :user_id
  # belongs_to :alarm
  # belongs_to :user
  # validates_inclusion_of :controller_generation, :in => ALL_CONTROLLER_GENERATIONS
  # validates_inclusion_of :robot_model, :in => ALL_ROBOT_MODELS
  # validates_inclusion_of :part_replaced, :in => ALL_PARTS_REPLACED, :allow_nil => true
    
  # def self.all_controller_generations
  #     ALL_CONTROLLER_GENERATIONS
  # end
  
  # def self.all_robot_models
  #     ALL_ROBOT_MODELS
  # end

  # def self.all_parts_replaced
  #     ALL_PARTS_REPLACED
  # end
end