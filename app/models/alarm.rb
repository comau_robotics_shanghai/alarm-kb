class Alarm
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :code, :type => Integer
  field :message, :type => String, localize: true
  field :severity, :type => String
  field :cause, :type => String, localize: true
  field :remedy, :type => String, localize: true
  field :comments, :type => String, localize: true
  
  validates_presence_of :code
  
  default_scope order_by(:created_at => :asc)
  
  key :code
  
  # belongs_to :diagnosis
  
  def self.all_severities
    %w(0-INFO 1-INFO 2-WARNING 4-PAUSE 8-HOLD 10-DRIVE\ OFF 11-DRIVE\ OFF 12-DRIVE\ OFF 13-FATAL 14-FATAL 15-FATAL )
  end
end

Alarm.all.cache