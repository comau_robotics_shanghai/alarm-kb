class Part
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :part_no, :type => String
  field :name, :type => String
  #field :replacement_pdf, :type => String
  
  has_many :manifests, autosave: true, dependent: :delete#, {:group => :robot_id}
  accepts_nested_attributes_for :manifests, :allow_destroy => true
  
  validates_presence_of :part_no
  validates_uniqueness_of :part_no
  
  default_scope order_by(:created_at => :asc)
  
  key :part_no
  
  #mount_uploader :replacement_pdf, ReplacementPdfUploader
  
  # has_and_belongs_to_many :diagnoses
  #belongs_to :replacedpart
  
  def part_no_and_name
    "#{part_no}: #{name}"
  end
end