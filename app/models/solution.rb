class Solution
  include Mongoid::Document

  field :part_no, :type => String
  field :times, :type => Integer
  field :description, :type => String, localize: true
  field :example, :type => String, localize: true
  
  #has_one :part
  #referenced_in :part
  embedded_in :diagnosis, :inverse_of => :solutions
  
  validates_presence_of :part_no
  
  #accepts_nested_attributes_for :part
end
