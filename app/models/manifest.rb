class Manifest
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :axis, :type => String
  field :replacement_pdf, :type => String
  
  belongs_to :robot
  belongs_to :part
    
  scope :group_by_robot, where(:group => :robot_id)
  scope :group_by_part, where(:group => :part_id)
    
  mount_uploader :replacement_pdf, ReplacementPdfUploader
    
end