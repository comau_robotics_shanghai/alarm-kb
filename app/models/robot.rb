class Robot
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :code, :type => String
  field :model, :model => String
  field :axes, :type => Hash
  
  has_many :manifests, autosave: true, dependent: :delete
  accepts_nested_attributes_for :manifests, :allow_destroy => true
  
  validates_presence_of :code
  validates_uniqueness_of :code
  
  default_scope order_by(:created_at => :asc)
  key :code
  
  def self.all_axes
    %w(1 2 3 4 5 6)
  end
  
  def code_and_model
    "#{code}: #{model}"
  end
  
  def axes_abbr
    if axes
      Robot.all_axes.map do |key|
        if (axes[key] == "1")
          key
        else
          "-"
        end
      end.join
    end
  end
  
end
