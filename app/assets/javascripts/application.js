// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require jquery.remotipart
//= require jquery.ui.effect-blind
//= require bootstrap
//= require_tree .
//= require bootstrap-datetimepicker

function remove_fields(link, is_embedded) {
  if (is_embedded) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".fields").hide();
  } else {
    $(link).closest(".fields").remove();
  }
}

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g");
  $(link).parent().before(content.replace(regexp, new_id));
}

function add_fields_to_table(table_id, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g");
  $(table_id).children('tbody').append(content.replace(regexp, new_id));
}

function add_fields2(link, content) {
  $(link).parent().before(content);
}

function HTMLEncode(str){
  var i = str.length,
      aRet = [];

  while (i--) {
    var iC = str[i].charCodeAt();
    if (iC < 65 || iC > 127 || (iC>90 && iC<97)) {
      aRet[i] = '&#'+iC+';';
    } else {
      aRet[i] = str[i];
    }
   }
  return aRet.join('');    
}

function check_part_replaced(checkbox) {
  if ($(checkbox).is(":checked")) {
	$(checkbox).parent().prev().prev().find("input").val(1);
  } else {
	$(checkbox).parent().prev().prev().find("input").val(0);
  }
}

$(document).ready(function() {
    $('#main #warning').animate({backgroundColor: '#FFFFFF'}, 3000);
    $('#main #alert').animate({backgroundColor: '#FFFFFF'}, 3000);
  }
)

