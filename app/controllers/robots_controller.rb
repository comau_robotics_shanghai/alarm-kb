class RobotsController < ApplicationController
  before_filter :check_for_existing_robot, :only => [:create]
  before_filter :authenticate_user!
  load_and_authorize_resource

  # GET /robots
  # GET /robots.json
  def index
    page = params[:page] || 1
    sort = params[:sort] || session[:sort]
    case sort
    when 'no'
      ordering,@no_header = :no, 'hilite'
    end
    
    @robots = Robot.all.order_by([ordering, :asc]).page(page).per(10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @robots }
    end
  end

  # GET /robots/1
  # GET /robots/1.json
  def show

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @robot }
    end
  end

  # GET /robots/new
  # GET /robots/new.json
  def new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @robot }
    end
  end

  # GET /robots/1/edit
  def edit
  end

  # POST /robots
  # POST /robots.json
  def create

    respond_to do |format|
      if @robot.save
        format.html { redirect_to @robot, notice: 'Robot was successfully created.' }
        format.json { render json: @robot, status: :created, location: @robot }
      else
        format.html { render action: "new" }
        format.json { render json: @robot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /robots/1
  # PUT /robots/1.json
  def update

    respond_to do |format|
      if @robot.update_attributes(params[:robot])
        format.html { redirect_to @robot, notice: 'Robot was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @robot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /robots/1
  # DELETE /robots/1.json
  def destroy
    @robot.destroy

    respond_to do |format|
      format.html { redirect_to robots_url }
      format.json { head :ok }
    end
  end
  
  def search
    code = params[:code]
    if code
      @robot = Robot.where(:code => code).first
      respond_to do |format|
        format.js
      end
    end
  end
  
  def axes
    respond_to do |format|
      format.json { render json: @robot.axes.keys }
    end
  end
  
  def check_for_existing_robot
    robot = Robot.where(:code => params[:robot][:code]).first
    if robot
      flash[:warning] = "Robot '#{robot.code}' already existed."
      redirect_to(robot) and return
    end
  end
end
