class DiagnosesController < ApplicationController
 before_filter :check_for_existing_diagnosis, :only => [:create]
 before_filter :authenticate_user!
 authorize_resource
 
 rescue_from CanCan::AccessDenied do |exception|
   redirect_to diagnosis_logfile_path
 end
 
 # caches_page :index
 
  def show
    @diagnosis = Diagnosis.find(params[:id]) # look up diagnosis by unique ID
    @alarms = @diagnosis.alarms.map { |code| Alarm.where(:code => code).first }
    #@solutions = @diagnosis.solutions.map { |solution| Part.where(:part_no => solution.part_no).first }
    @solutions_aggregated = {}
    all_times = zero_times = 0
    @diagnosis.solutions.each do |s|
      name = Part.where(:part_no => s.part_no).first.name
      if @solutions_aggregated[name]
        @solutions_aggregated[name] += s.times
      else
        @solutions_aggregated[name] = s.times
      end
      if s.times != 0
        all_times += s.times
      end
    end
    zero_times = @solutions_aggregated.select {|k, v| v == 0}.size + 1  # + 1 for the other
    @solutions_aggregated.each do |k, v|
      if v == 0
        @solutions_aggregated[k] = '1%'
      else
        percentage = (v * 1.0 / all_times) * (1 - zero_times * 1.0 / 100) * 100
        @solutions_aggregated[k] = "#{percentage.round(2)}%"
      end
    end
    # will render app/views/diagnoses/show.<extension> by default
  end

  def index
    per_page = 10
    page = params[:page] || 1
    @diagnoses = Diagnosis.all.page(page).per(per_page)
    @no = (page.to_i - 1) * per_page
  end

  def new
    # default: render 'new' template
    alarms = params[:alarms]
    @diagnosis = Diagnosis.new(:alarms => alarms)
    @diagnosis.solutions.build
  end

  def create
    @diagnosis = Diagnosis.new(params[:diagnosis])
    if @diagnosis.save
      flash[:notice] = "Successfully created diagnosis."
      redirect_to diagnosis_path(@diagnosis)
    else
      render :action => 'new'
    end
  end

  def edit
    @diagnosis = Diagnosis.find params[:id]
    @alarms = @diagnosis.alarms.map { |code| Alarm.where(:code => code).first }
  end

  def update
    @diagnosis = Diagnosis.find params[:id]
    @diagnosis.update_attributes!(params[:diagnosis])
    flash[:notice] = "Diagnosis '#{@diagnosis.id}' was successfully updated."
    redirect_to diagnosis_path(@diagnosis)
  end

  def destroy
    @diagnosis = Diagnosis.find params[:id]
    @diagnosis.destroy
    flash[:notice] = "Diagnosis '#{@diagnosis.id}' deleted."
    redirect_to diagnoses_path
  end

  def search
    alarms = params[:alarms]
    if alarms
      @diagnoses = Diagnosis.all_in(:alarms => alarms).select{ |d| (alarms & d.alarms) == alarms }
      
      per_page = 10
      page = params[:page] || 1
      @no = (page.to_i - 1) * per_page
      
      Kaminari.paginate_array(@diagnoses).page(page).per(per_page)
      respond_to do |format|
        format.js
      end
    end
  end
  
  def logfile
  end
  
  def analyze
    #if params[:date] and params[:time] and params[:range] and params[:log]
      respond_to do |format|
        #format.html
        format.js do
          # parse the params
          str_date = Date.parse(params[:date]).strftime('%d-%^b-%y')
          date_time = DateTime.strptime(params[:date] + " " + params[:time], '%Y-%m-%d %H:%M:%S')
          range = params[:range]
          contents = File.read(params[:log].tempfile)

          @solution_results = []
          @no_solution_results = []
          @alarms_happened_time = {} # to record each alarm_codes and their happened time
          #idx = contents.index str_date
          #if idx
          #  contents = contents[idx, contents.size - idx - 1]

            time_diff = case 
                          when range == "1" then 5
                          when range == "2" then 10
                          when range == "3" then 15
                          else 0
                        end * 60
            regex = /(?<datetime>[A-Za-z0-9\-\s\:]+)(\s)(\d+)(\.)(?<code>\d+)(\-)(?<severity>\d+)(\s\:\s)(?<message>.+)/
            prev_date = nil
            prev_str_date = ""
            messages = ""
            alarm_codes = []
            contents.each_line do |l|
              next if (regex =~ l).nil?
              data = Regexp.last_match
              str_dt = data[:datetime]
              dt = DateTime.parse(str_dt)
              severity = data[:severity]

              next if dt - date_time < 0 # skip all alarms before the specified time

              if time_diff != 0 and (((dt - date_time) * 1.days).to_i > time_diff) # stop iterating when error happened after the time range
                size = alarm_codes.size
                if size > 0
                  if @alarms_happened_time[alarm_codes]
                    @alarms_happened_time[alarm_codes] << prev_str_date
                  else
                    @alarms_happened_time[alarm_codes] = [prev_str_date]
                    #diagnosis = Diagnosis.where(:alarms.sort => alarm_codes.sort).first # Find the corresponding parts replaced in the DB
                    diagnosis = Diagnosis.where(:alarms => alarm_codes).first # Find the corresponding parts replaced in the DB
                    if diagnosis
                      @solution_results = @solution_results << [alarm_codes, messages, diagnosis]
                    else
                      @no_solution_results = @no_solution_results << [alarm_codes, messages, diagnosis]
                    end
                  end
                end
                break
              end

              next if severity == "00" or severity == "01" or severity == "02" # skip all infos and warnings

              code, message = data[:code], CGI::escapeHTML(data[:message])

              if prev_date != dt
                if !prev_date.nil?
                  size = alarm_codes.size
                  if size > 0
                    if @alarms_happened_time[alarm_codes]
                      @alarms_happened_time[alarm_codes] << prev_str_date
                    else
                      @alarms_happened_time[alarm_codes] = [prev_str_date]
                      #diagnosis = Diagnosis.where(:alarms.sort => alarm_codes.sort).first # Find the corresponding parts replaced in the DB
                      diagnosis = Diagnosis.where(:alarms => alarm_codes).first # Find the corresponding parts replaced in the DB
                      if diagnosis
                        @solution_results = @solution_results << [alarm_codes, messages, diagnosis]
                      else
                        @no_solution_results = @no_solution_results << [alarm_codes, messages, diagnosis]
                      end
                    end
                  end
                end
                prev_date, prev_str_date, alarm_codes, messages = dt, str_dt, [code], "#{code} #{message}"
              else
                alarm_codes = alarm_codes << code
                messages = messages + "<br>" + "#{code} #{message}"
              end
            end
          #end
          
          if !current_user.admin?
            error_log = ErrorLog.new
            error_log.delay.persist_and_send_mail(current_user.id, params[:date], params[:time], params[:range], contents, params[:log].original_filename)
          end
        end
      end
    #end
  end
  
  def part
    @part_name = params[:part]
  end
  
  def check_for_existing_diagnosis
    diagnosis = Diagnosis.where(:alarms => params[:diagnosis][:alarms]).first
    if diagnosis
      flash[:warning] = "Diagnosis '#{diagnosis.alarms}' already existed."
      redirect_to(diagnosis) and return
    end
  end
  
  def self.parse_error_log(str_date, date_time, range, log_file)
    
  end
  
end
