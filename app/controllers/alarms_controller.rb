class AlarmsController < ApplicationController
  before_filter :check_for_existing_alarm, :only => [:create]
  before_filter :authenticate_user!, :except => [:show, :index, :search]
  authorize_resource :only => [:new, :create, :edit, :update, :destroy]
 # caches_page :index
 
  def show
    @alarm = Alarm.where(:_id => params[:id].to_i).first # look up alarm by unique ID
    # will render app/views/alarms/show.<extension> by default
  end

  def index
    page = params[:page] || 1
    sort = params[:sort] || session[:sort]
    case sort
    when 'code'
      ordering,@code_header = :code, 'hilite'
    when 'severity'
      ordering,@severity_header = :severity, 'hilite'
    end
    @all_severities = Alarm.all_severities
    @selected_severities = params[:severities] || session[:severities] || {}

    if @selected_severities == {}
      @selected_severities = Hash[@all_severities.map {|severity| [severity, severity]}]
    end
    
    if params[:sort] != session[:sort]
      session[:sort] = sort
      flash.keep
      redirect_to :sort => sort, :severities => @selected_severities and return
    end

    if params[:severities] != session[:severities] and @selected_severities != {}
      session[:sort] = sort
      session[:severities] = @selected_severities
      flash.keep
      redirect_to :sort => sort, :severities => @selected_severities and return
    end
    @alarms = Alarm.any_in(severity: @selected_severities.keys).order_by([ordering, :asc]).page(page).per(10)
  end

  def new
    # default: render 'new' template
  end

  def create
    @alarm = Alarm.create!(params[:alarm])
    flash[:notice] = "Alarm '#{@alarm.code}' was successfully created."
    redirect_to alarms_path
  end

  def edit
    @alarm = Alarm.where(:_id => params[:id].to_i).first
  end

  def update
    @alarm = Alarm.where(:_id => params[:id].to_i).first
    @alarm.update_attributes!(params[:alarm])
    flash[:notice] = "Alarm '#{@alarm.code}' was successfully updated."
    redirect_to alarm_path(@alarm)
  end

  def destroy
    @alarm = Alarm.where(:_id => params[:id].to_i).first
    @alarm.destroy
    flash[:notice] = "Alarm '#{@alarm.code}' deleted."
    redirect_to alarms_path
  end
  
  def search
    code = params[:code]
    if code
      @alarm = Alarm.where(:code => code).first
      respond_to do |format|
        format.js
        format.json {
          render :json => @alarm 
        }
      end
    end
  end
  
  def check_for_existing_alarm
    alarm = Alarm.where(:code => params[:alarm][:code]).first
    if alarm
      flash[:warning] = "Alarm '#{alarm.code}' already existed."
      redirect_to(alarm) and return
    end
  end
end
