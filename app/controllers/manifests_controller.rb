class ManifestsController < ApplicationController
  def get_replacement_by_part_name
    part_ids = Part.where(:name => params[:part_name]).map { |p| p.id }
    @manifest = Manifest.where(:robot_id => params[:robot_id], :axis => params[:axis], :part_id => {"$in" => part_ids}).first
    render :partial => "replacement", :locals => { :manifest => @manifest }
  end
end