class PartsController < ApplicationController
  before_filter :check_for_existing_part, :only => [:create]
  before_filter :authenticate_user!
  load_and_authorize_resource
 # caches_page :index
 
  def show
    #id = params[:id] # retrieve part ID from URI route
    #@part = Part.find(id) # look up part by unique ID
    # will render app/views/parts/show.<extension> by default
  end

  def index
    page = params[:page] || 1
    sort = params[:sort] || session[:sort]
    case sort
    when 'no'
      ordering,@no_header = :no, 'hilite'
    end
    
    @parts = Part.all.order_by([ordering, :asc]).page(page).per(10)
  end

  def new
    #@part = Part.new
    
    # default: render 'new' template
  end

  def create
    #@part = Part.create!(params[:part])
    if @part.save
      flash[:notice] = "Part '#{@part.part_no}' was successfully created."
      redirect_to parts_path
    else
      flash[:notice] = "Part '#{params[:part]}' was already existed."
      redirect_to parts_path
    end
  end

  def edit
    #@part = Part.find params[:id]
  end

  def update
    #@part = Part.find params[:id]
    @part.update_attributes!(params[:part])
    flash[:notice] = "Part '#{@part.part_no}' was successfully updated."
    redirect_to part_path(@part)
  end

  def destroy
    #@part = Part.find(params[:id])
    @part.destroy
    flash[:notice] = "Part '#{@part.part_no}' deleted."
    redirect_to parts_path
  end
  
  def search
    part_no = params[:part_no]
    if part_no
      @part = Part.where(:part_no => part_no).first
      respond_to do |format|
        format.js
      end
    end
  end
  
  def check_for_existing_part
    part = Part.where(:part_no => params[:part][:part_no]).first
    if part
      flash[:warning] = "Part '#{part.part_no}' already existed."
      redirect_to(part) and return
    end
  end
end
