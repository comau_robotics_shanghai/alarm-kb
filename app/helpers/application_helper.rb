module ApplicationHelper
  def link_to_remove_fields(name, f = nil)
    if f
      f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this, true)")
    else
      link_to_function(name, "remove_fields(this, false)")
    end
  end
  
  def link_to_add_fields(name, f, association, options = {}, table_id = "")
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render :partial => association.to_s.singularize + "_fields", :locals => {:f => builder, :opt => options}
    end
    if table_id.empty?
      link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
    else
      link_to_function(name, "add_fields_to_table(\"#{table_id}\", \"#{association}\", \"#{escape_javascript(fields)}\")")
    end
  end
  
  def link_to_add_alarms(name, f, association)
    fields = h(render "alarm_fields", :f => f, :id => "new_#{association}", :alarm => "")
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  
  def link_to_add_alarms_without_parent(name)
    fields = h(render "alarm_fields_without_parent")
    link_to_function(name, "add_fields2(this, \"#{escape_javascript(fields)}\")")
  end
  
  def add_alarm_link(name)
    # This is a trick I picked up... if you don't feel like installing a
    # javascript templating engine, or something like Jammit, but still want
    # to render more than very simple html using JS, you can render a partial
    # into one of the data-attributes on the element.
    #
    # Using Jammit's JST abilities may be worthwhile if it's something you do
    # frequently though.
    link_to name, "#", "data-partial" => h(render(:partial => 'alarm', :object => Alarm.new) + ''), :class => 'add_alarm'
  end
end
