class LogAnalyzerMailer < ActionMailer::Base  
  default from: "jiangkang1982@gmail.com"
  
  def analyze_log_email(user_id, date, time, range, original_filename, log_url)
    @user = User.find(user_id)
    #@analyze_params = analyze_params
    @date = date
    @time = time
    @range = range
    @original_filename = original_filename
    @log_url = log_url
    #attachments[original_filename] = File.read(tempfile)
    mail(to: User.admin.email, subject: "Analyze logfile report #{Time.now}")
  end
end
